<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*use App\SanPham;
Route::get('thu',function(){
	$sanpham= sanpham::find(1);
	foreach($sanpham->loaisanpham as $loaisanpham)
	{
		echo $loaisanpham->Ten."<br>";
	}
});
Route::get('testview',function(){
	return view('admin.tintuc.danhsach');
});*/
Route::get('admin/dangnhap','UserController@getdangnhapAdmin');
Route::post('admin/dangnhap','UserController@postdangnhapAdmin');
Route::get('admin/logout','UserController@gettdangxuatAdmin');
Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function(){
	Route::group(['prefix'=>'sanpham'],function(){
		Route::get('danhsach','SanPhamController@getDanhSach');
		Route::get('sua/{id}','SanPhamController@getSua');
		Route::post('sua/{id}','SanPhamController@postSua');
		Route::get('them','SanPhamController@getThem');
		Route::post('them','SanPhamController@postThem');
		Route::get('xoa/{id}','SanPhamController@getXoa');
	});

	Route::group(['prefix'=>'loaisanpham'],function(){
		Route::get('danhsach','LoaiSanPhamController@getDanhSach');
		Route::get('sua/{id}','LoaiSanPhamController@getSua');
		Route::post('sua/{id}','LoaiSanPhamController@postSua');
		Route::get('them','LoaiSanPhamController@getThem');
		Route::post('them','LoaiSanPhamController@postThem');
		Route::get('xoa/{id}','LoaiSanPhamController@getXoa');
	});

	Route::group(['prefix'=>'tintuc'],function(){
		Route::get('danhsach','TinTucController@getDanhSach');
		Route::get('sua/{id}','TinTucController@getSua');
		Route::post('sua/{id}','TinTucController@postSua');
		Route::get('them','TinTucController@getThem');
		Route::post('them','TinTucController@postThem');
		Route::get('xoa/{id}','TinTucController@getXoa');
	});
	Route::group(['prefix'=>'comment'],function(){
		Route::get('xoa/{id}/{idtintuc}','CommentController@getXoa');

	});
	Route::group(['prefix'=>'slide'],function(){
		Route::get('danhsach','SlideController@getDanhSach');
		Route::get('sua/{id}','SlideController@getSua');
		Route::post('sua/{id}','SlideController@postSua');
		Route::get('them','SlideController@getThem');
		Route::post('them','SlideController@postThem');
		Route::get('xoa/{id}','SlideController@getXoa');
	});
	Route::group(['prefix'=>'user'],function(){
		Route::get('danhsach','UserController@getDanhSach');
		Route::get('sua/{id}','UserController@getSua');
		Route::post('sua/{id}','UserController@postSua');
		Route::get('them','UserController@getThem');
		Route::post('them','UserController@postThem');
		Route::get('xoa/{id}','UserController@getXoa');
	});
	Route::group(['prefix'=>'tinhtrang'],function(){
		Route::get('danhsach','TinhtrangController@getDanhSach');
		Route::get('sua/{id}','TinhtrangController@getSua');
		Route::post('sua/{id}','TinhtrangController@postSua');
		Route::get('them','TinhtrangController@getThem');
		Route::post('them','TinhtrangController@postThem');
		Route::get('xoa/{id}','TinhtrangController@getXoa');
	});
	Route::group(['prefix'=>'thongtindiemdo'],function(){
		Route::get('danhsach','ThongTinDiemDoController@getDanhSach');
		Route::get('sua/{id}','ThongTinDiemDoController@getSua');
		Route::post('sua/{id}','ThongTinDiemDoController@postSua');
		Route::get('them','ThongTinDiemDoController@getThem');
		Route::post('them','ThongTinDiemDoController@postThem');
		Route::get('xoa/{id}','ThongTinDiemDoController@getXoa');
	});
	Route::group(['prefix'=>'ketquado'],function(){
		Route::get('danhsach','KetQuaDoController@getDanhSach');
		Route::get('sua/{id}','KetQuaDoController@getSua');
		Route::post('sua/{id}','KetQuaDoController@postSua');
		Route::get('them','KetQuaDoController@getThem');
		Route::post('them','KetQuaDoController@postThem');
		Route::get('xoa/{id}','KetQuaDoController@getXoa');
	});
	Route::group(['prefix'=>'ajax'],function(){
		Route::get('loaisanpham/{idsanpham}','AjaxController@getLoaiSanPham');
	});
});


Route::get('trangchu','PageController@trangchu');
Route::get('lienhe','PageController@lienhe');
Route::get('loaisanpham/{id}/{TenKhongDau}.html','PageController@loaisanpham');
Route::get('tintuc/{id}/{TieuDeKhongDau}.html','PageController@tintuc');

Route::get('dangnhap','PageController@getdangnhap');
Route::post('dangnhap','PageController@postdangnhap');
Route::get('dangxuat','PageController@getdangxuat');
Route::get('nguoidung','PageController@getnguoidung');
Route::post('nguoidung','PageController@postnguoidung');
Route::get('dangky','PageController@getdangky');
Route::post('dangky','PageController@postdangky');


Route::post('comment/{id}','CommentController@postcomment');
Route::post('timkiem','PageController@timkiem');
Route::get('thongso','PageController@thongso');
Route::get('lienhe','PageController@lienhe');
Route::get('gioithieu','PageController@gioithieu');
Route::get('thuu', function () {
   $data = DB::table('users')->get();
   foreach ($data as $row) 
   {
   	foreach ($row as $key => $value)
   	 {
   	 	$key->get('id',2);
    	echo $key.":".$value."<br>";    
     } 
    	echo "<hr>";
   }
  
});
Route::get('testt','PageController@teeee');