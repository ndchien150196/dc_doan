-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 08, 2019 lúc 12:37 PM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `dinhchien`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `iduser` int(10) UNSIGNED NOT NULL,
  `idtintuc` int(10) UNSIGNED NOT NULL,
  `NoiDung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comment`
--

INSERT INTO `comment` (`id`, `iduser`, `idtintuc`, `NoiDung`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'ngon lắm ', '2018-04-24 04:20:20', '2018-04-24 14:30:45'),
(2, 1, 1, 'ăn được', '2018-04-24 04:17:46', '2018-04-24 04:17:46'),
(3, 1, 2, 'ahihi', '2018-04-24 05:49:37', '2018-04-24 05:49:37'),
(4, 12, 25, 'ăn rất ngon', '2018-05-02 21:40:18', '2018-05-02 21:40:18'),
(5, 13, 19, 'Sản phẩm rất tốt . Chúc bán hàng tốt', '2018-05-03 11:03:00', '2018-05-03 11:03:00'),
(6, 1, 25, 'ăn ngon', '2018-05-07 00:55:36', '2018-05-07 00:55:36'),
(7, 14, 25, 'ăn được', '2018-05-07 00:57:27', '2018-05-07 00:57:27'),
(8, 14, 25, 'ăn được', '2018-05-07 00:57:27', '2018-05-07 00:57:27'),
(9, 1, 26, 'ngon', '2018-05-07 00:59:14', '2018-05-07 00:59:14'),
(10, 15, 25, 'tốt', '2018-05-07 01:00:58', '2018-05-07 01:00:58'),
(11, 15, 19, 'Cũng được', '2018-05-07 01:09:37', '2018-05-07 01:09:37'),
(12, 16, 40, 'Chờ giảm giá sẽ mua', '2018-05-07 01:10:48', '2018-05-07 01:10:48'),
(13, 17, 41, 'chất lượng', '2018-05-07 02:27:42', '2018-05-07 02:27:42'),
(14, 15, 19, 'Đẹp', '2018-05-07 03:18:29', '2018-05-07 03:18:29'),
(15, 2, 27, 'Ăn rất ngon', '2018-05-07 19:11:07', '2018-05-07 19:11:07');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ketquado`
--

CREATE TABLE `ketquado` (
  `id` int(10) UNSIGNED NOT NULL,
  `nhietdo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doam` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anhsang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `co` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buipm1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buipm25` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idthongtindiemdo` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ketquado`
--

INSERT INTO `ketquado` (`id`, `nhietdo`, `doam`, `anhsang`, `co`, `buipm1`, `buipm25`, `pin`, `mota`, `idthongtindiemdo`, `created_at`, `updated_at`) VALUES
(1, '26 *C', '20 %', '60 lux', '20 ppb', '30 pm', '20 pm', '90 %', 'Hoạt Động Tốt', 1, '2018-04-27 01:34:35', '2018-05-02 12:58:55'),
(2, '26 *C', '20 %', '60 lux', '40 ppb', '50 pm', '60 pm', '90 %', 'An Toàn', 5, '2018-04-28 23:48:01', '2018-05-02 13:45:57'),
(3, '26 *C', '20 %', '60 lux', '17 ppb', '20 pm', '30 pm', '90 %', 'An Toàn', 6, '2018-04-29 09:28:58', '2018-05-02 13:46:18'),
(4, '25 *C', '20 %', '60 lux', '10 ppb', '20 pm', '30 pm', '90 %', 'An Toàn', 3, '2018-04-29 09:44:36', '2018-05-02 13:14:29'),
(5, '24 *C', '42 %', '60 lux', '24 ppb', '43 pm', '34 pm', '90 %', 'An Toàn', 4, '2018-05-02 13:16:49', '2018-05-02 13:16:49'),
(6, '26 *C', '35 %', '60 lux', '56 ppb', '23 pm', '34 pm', '90 %', 'An Toàn ', 7, '2018-05-03 06:38:38', '2018-05-03 08:34:37'),
(7, '20 *C', '20 %', '60 lux', '34 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 2, '2018-05-02 13:20:38', '2018-05-02 13:20:38'),
(8, '26 *C', '20 %', '60 lux', '35 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 1, '2018-05-02 13:23:16', '2018-05-02 13:23:16'),
(9, '26 *C', '20 %', '60 lux', '34 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 5, '2018-05-02 13:23:37', '2018-05-02 13:23:37'),
(10, '22 *C', '20 %', '60 lux', '32 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 6, '2018-05-02 13:23:52', '2018-05-02 13:23:52'),
(11, '26 *C', '20 %', '60 lux', '34 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 3, '2018-05-02 13:25:05', '2018-05-02 13:25:05'),
(12, '26 *C', '20 %', '60 lux', '37 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 4, '2018-05-02 13:25:23', '2018-05-02 13:25:23'),
(13, '26 *C', '20 %', '60 lux', '34 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 7, '2018-05-02 13:25:40', '2018-05-02 13:25:40'),
(14, '20 *C', '20 %', '60 lux', '36 ppb', '50 pm', '30 pm', '90 %', 'An Toàn', 2, '2018-05-02 13:27:45', '2018-05-02 13:27:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loaisanpham`
--

CREATE TABLE `loaisanpham` (
  `id` int(10) UNSIGNED NOT NULL,
  `idsanpham` int(10) UNSIGNED NOT NULL,
  `Ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TenKhongDau` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loaisanpham`
--

INSERT INTO `loaisanpham` (`id`, `idsanpham`, `Ten`, `TenKhongDau`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dây Điện', 'Day Dien', '2018-04-23 03:25:25', '2018-04-23 04:31:33'),
(2, 4, 'Kimpap', 'kimpap', '2018-04-22 11:53:58', '2018-04-22 11:53:58'),
(4, 3, 'Kem', 'kem', '2018-04-22 12:45:22', '2018-04-22 12:47:19'),
(5, 4, 'Kimchi', 'kimchi', '2018-04-22 12:46:12', '2018-04-22 12:46:12'),
(6, 6, 'Sushi', 'sushi', '2018-04-22 12:48:16', '2018-04-22 12:48:16'),
(7, 3, 'Kem Hàn Quốc', 'kem-han-quoc', '2018-04-23 02:49:37', '2018-04-23 02:49:37'),
(8, 4, 'Tokbokki', 'tokbokki', '2018-04-23 20:41:21', '2018-04-23 20:41:21'),
(9, 4, 'GoGi', 'gogi', '2018-04-23 20:43:36', '2018-04-23 20:43:36'),
(10, 5, 'Đồ Hộp Handmade', 'do-hop-handmade', '2018-04-24 05:50:38', '2018-04-24 05:50:38'),
(11, 1, 'Ổ Cắm', 'o-cam', '2018-04-26 00:08:09', '2018-04-26 00:08:09'),
(14, 5, 'Cá Hộp', 'ca-hop', '2018-04-26 00:08:41', '2018-04-26 00:08:41'),
(15, 5, 'Thịt Hộp', 'thit-hop', '2018-04-26 00:09:10', '2018-04-26 00:09:10'),
(16, 7, 'Đậu Hũ Thối', 'dau-hu-thoi', '2018-04-27 01:44:19', '2018-04-27 01:44:19'),
(17, 1, 'Đèn LED', 'den-led', '2018-05-02 12:06:58', '2018-05-02 12:06:58'),
(18, 1, 'Đèn pin LED', 'den-pin-led', '2018-05-02 12:11:12', '2018-05-02 12:11:12'),
(19, 1, 'Đèn pin LED 7', 'den-pin-led-7', '2018-05-02 12:16:00', '2018-05-02 12:16:00'),
(20, 2, 'Bàng khô', 'bang-kho', '2018-05-02 12:18:21', '2018-05-02 12:18:21'),
(21, 2, 'Khô bò', 'kho-bo', '2018-05-02 12:18:41', '2018-05-02 12:18:41'),
(22, 2, 'Nho khô', 'nho-kho', '2018-05-02 12:18:52', '2018-05-02 12:18:52'),
(23, 2, 'Khô heo', 'kho-heo', '2018-05-02 12:19:07', '2018-05-02 12:19:07'),
(24, 6, 'Nệm thú bông', 'nem-thu-bong', '2018-05-02 12:28:12', '2018-05-02 12:28:12'),
(25, 6, 'Tinh chất', 'tinh-chat', '2018-05-02 12:28:32', '2018-05-02 12:28:32'),
(26, 7, 'Mì dan dan', 'mi-dan-dan', '2018-05-02 12:37:35', '2018-05-02 12:37:35'),
(27, 7, 'Bánh Bao', 'banh-bao', '2018-05-02 12:38:41', '2018-05-02 12:38:41'),
(28, 7, 'Miến chua ngọt', 'mien-chua-ngot', '2018-05-02 12:39:41', '2018-05-02 12:39:41'),
(29, 3, 'Bánh kem', 'banh-kem', '2018-05-02 12:47:38', '2018-05-02 12:47:38'),
(30, 13, 'louboutin', 'louboutin', '2018-05-07 00:59:37', '2018-05-07 00:59:37'),
(31, 11, 'Áo trẻ em', 'ao-tre-em', '2018-05-07 02:25:59', '2018-05-07 02:25:59');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_04_22_025954_create_sanpham_table', 1),
(3, '2018_04_22_040905_create_loaisanpham_table', 2),
(4, '2018_04_22_041514_create_tintuc_table', 2),
(5, '2018_04_22_030323_create_slide_table', 3),
(6, '2018_04_22_030300_create_comment_table', 4),
(7, '2018_04_22_042331_create_tinhtrang_table', 5),
(8, '2018_04_22_042359_create_thongtindiemdo_table', 5),
(9, '2018_04_22_042449_create_ketquado_table', 5),
(10, '2018_04_22_044815_create_tinhtrang_table', 6),
(11, '2018_04_22_045727_create_tinhtrang_table', 7),
(12, '2018_04_22_045907_create_thongtindiemdo_table', 8),
(13, '2018_04_22_050523_create_ketquado_table', 9),
(14, '2018_04_22_050917_create_thongtindiemdo_table', 10),
(15, '2018_04_22_050940_create_ketquado_table', 10);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `id` int(10) UNSIGNED NOT NULL,
  `Ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TenKhongDau` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`id`, `Ten`, `TenKhongDau`, `created_at`, `updated_at`) VALUES
(1, 'Đồ Điện Dân Dụng', 'do-dien-dan-dung', '2018-04-22 00:13:21', '2018-05-02 11:52:22'),
(2, 'Đồ Khô', 'do-kho', '2018-04-22 07:56:42', '2018-04-22 07:56:42'),
(3, 'Đồ Đông Lạnh', 'do-dong-lanh', '2018-04-22 07:57:45', '2018-04-22 07:57:45'),
(4, 'Đồ Hàn Quốc', 'do-han-quoc', '2018-04-22 07:58:34', '2018-04-22 07:58:34'),
(5, 'Đồ Hộp', 'do-hop', '2018-04-22 09:57:22', '2018-04-22 09:57:22'),
(6, 'Đồ Nhật Bản', 'do-nhat-ban', '2018-04-22 09:43:12', '2018-04-22 09:44:23'),
(7, 'Đồ Trung Quốc', 'do-trung-quoc', '2018-04-23 02:49:18', '2018-04-23 02:49:18'),
(8, 'Đồ Điện Tử', 'do-dien-tu', '2018-04-26 00:06:44', '2018-04-26 00:06:44'),
(9, 'Đồ Tự Làm', 'do-tu-lam', '2018-04-26 00:07:20', '2018-04-26 00:07:20'),
(10, 'Món Ăn', 'mon-an', '2018-04-27 01:43:56', '2018-04-27 01:43:56'),
(11, 'Đồ May Mặc', 'do-may-mac', '2018-05-02 11:39:18', '2018-05-02 11:39:18'),
(12, 'Đồ Nội Thất', 'do-noi-that', '2018-05-02 11:39:27', '2018-05-02 11:39:27'),
(13, 'Mỹ Phẩm', 'my-pham', '2018-05-07 00:59:28', '2018-05-07 19:16:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(10) UNSIGNED NOT NULL,
  `Ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Hinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NoiDung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `Ten`, `Hinh`, `NoiDung`, `link`, `created_at`, `updated_at`) VALUES
(5, 'Gian hàng', 'X5kZ_1436864778-my14_ddau.jpg', '<p>thêm</p>', 'https://www.facebook.com/', '2018-05-06 22:31:22', '2018-05-06 22:31:22'),
(6, 'gian hàng 1', 'E0zx_ban-hang-sieu-thi3.jpg', '<p>gian hàng</p>', 'https://www.facebook.com/', '2018-05-06 22:31:41', '2018-05-06 22:31:41'),
(7, 'gian hàng 2', 'nmt6_foody-mobile-c2-jpg-944-636306966592873729.jpg', '<p>hàng</p>', 'https://www.facebook.com/', '2018-05-06 22:32:00', '2018-05-06 22:32:00'),
(8, 'thảo', 'iGVu_32072710_2118957551674109_2775490093824081920_n.png', '<p>thảo thích</p>', 'https://www.facebook.com/HOU-Confessions-572808712746737/', '2018-05-06 22:43:09', '2018-05-06 22:43:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `thongtindiemdo`
--

CREATE TABLE `thongtindiemdo` (
  `id` int(10) UNSIGNED NOT NULL,
  `tendiemdo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idtinhtrang` int(10) UNSIGNED NOT NULL,
  `mota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `thongtindiemdo`
--

INSERT INTO `thongtindiemdo` (`id`, `tendiemdo`, `idtinhtrang`, `mota`, `created_at`, `updated_at`) VALUES
(1, 'Hàng Khô', 1, 'An Toàn', '2018-04-27 06:22:00', '2018-04-28 04:31:38'),
(2, 'Hàng Đông Lạnh', 2, 'An Toàn', '2018-04-28 04:27:08', '2018-04-28 04:33:47'),
(3, 'Hàng Nhật', 3, 'An Toàn', '2018-04-28 04:33:26', '2018-04-28 04:34:41'),
(4, 'Hàng Hàn', 4, 'An Toàn', '2018-05-02 13:08:00', '2018-05-02 13:08:00'),
(5, 'Hàng Trung', 5, 'An Toàn', '2018-05-02 13:08:32', '2018-05-02 13:08:32'),
(6, 'Hàng Hộp', 6, 'An Toàn', '2018-05-02 13:08:57', '2018-05-02 13:08:57'),
(7, 'Hàng Điện', 7, 'An Toàn', '2018-05-02 13:09:16', '2018-05-02 13:09:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tinhtrang`
--

CREATE TABLE `tinhtrang` (
  `id` int(10) UNSIGNED NOT NULL,
  `dht11` int(255) DEFAULT '0',
  `spec` int(255) NOT NULL DEFAULT '0',
  `dust` int(255) NOT NULL DEFAULT '0',
  `bh1750` int(255) NOT NULL DEFAULT '0',
  `mota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tinhtrang`
--

INSERT INTO `tinhtrang` (`id`, `dht11`, `spec`, `dust`, `bh1750`, `mota`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 'Tốt', '2018-04-26 06:22:00', '2018-05-02 13:05:16'),
(2, 1, 0, 1, 1, 'đang sửa spec', '2018-04-26 00:31:00', '2018-04-27 02:43:11'),
(3, 1, 1, 1, 1, 'Tốt', '2018-04-27 02:07:42', '2018-04-27 02:39:39'),
(4, 1, 1, 1, 1, 'Tốt', '2018-05-02 13:05:45', '2018-05-02 13:05:45'),
(5, 1, 1, 1, 1, 'Tốt', '2018-05-02 13:05:50', '2018-05-02 13:05:50'),
(6, 1, 1, 1, 1, 'Tốt', '2018-05-02 13:05:54', '2018-05-02 13:05:54'),
(7, 1, 1, 1, 1, 'Tốt', '2018-05-02 13:06:12', '2018-05-02 13:06:12');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintuc`
--

CREATE TABLE `tintuc` (
  `id` int(10) UNSIGNED NOT NULL,
  `TieuDe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TieuDeKhongDau` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TomTat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `NoiDung` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Hinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NoiBat` int(11) NOT NULL DEFAULT '0',
  `SoLuotXem` int(11) NOT NULL DEFAULT '0',
  `idloaisanpham` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tintuc`
--

INSERT INTO `tintuc` (`id`, `TieuDe`, `TieuDeKhongDau`, `TomTat`, `NoiDung`, `Hinh`, `NoiBat`, `SoLuotXem`, `idloaisanpham`, `created_at`, `updated_at`) VALUES
(1, 'Kimpap', 'kimpap', 'Ngon', '<p>Kimpap Tự Làm</p>\r\n\r\n<p>L<span style=\"color:rgb(34, 34, 34); font-family:sans-serif; font-size:14px\">àm bằng cơm và các thành phần khác nhau cuộn trong lá rong biển khô (</span><em>nori</em><span style=\"color:rgb(34, 34, 34); font-family:sans-serif; font-size:14px\">). Người Triều Tiên thường làm kimbap để mang đi ăn trong những buổi dã ngoại hoặc các sự kiện ngoài trời, hoặc là trong các bữa ăn trưa nhẹ.&nbsp;</span></p>', '1IOn_gMPv_31166835_365731223922034_9139421622539649024_n.jpg', 1, 0, 2, '2018-04-23 20:34:34', '2018-05-02 11:46:28'),
(2, 'kem ngon', 'kem-ngon', 'kem', '<p>kem</p>', 'NpOp_PUMg_sp2.png', 1, 0, 4, '2018-04-23 20:38:19', '2018-04-23 20:38:19'),
(3, 'kimpap chiên', 'kimpap-chien', 'chiên', '<p>chiên</p>', 'AVws_1pDg_sushi-dau-hu-500.jpg', 1, 0, 2, '2018-04-23 20:39:10', '2018-04-23 20:39:10'),
(12, 'kimchi hàn quốc', 'kimchi-han-quoc', 'kimchi', '<p>kimchi</p>', '494r_tiCi_images.jpg', 1, 0, 5, '2018-04-23 20:40:29', '2018-04-23 20:40:29'),
(13, 'tokbokki', 'tokbokki', 'tokbokki', '<p>tokbokki</p>', '8jaN_cach-lam-tokbokki-2.jpg', 1, 0, 8, '2018-04-23 20:41:38', '2018-04-23 20:41:38'),
(14, 'compo', 'compo', 'tokbokki', '<p>tokbokki</p>', '0gtg_kimbap-thit-bo-tokbokki-pho-mai-1465839020-292676-1465839020.jpg', 0, 0, 8, '2018-04-23 20:42:51', '2018-04-27 01:34:29'),
(15, 'gogi nướng', 'gogi-nuong', 'gogi nướng', '<p>gogi nướng</p>', 'lm0Y_foody-mobile-gogi-house-sc-jpg-853-635733790565208144.jpg', 1, 0, 9, '2018-04-23 20:44:07', '2018-04-23 20:44:07'),
(16, 'Sushi tươi ngon', 'sushi-tuoi-ngon', 'Sushi cá hồi', '<p>Cá Hồi tươi sống</p>', 'VBB7_maxresdefault.jpg', 1, 0, 6, '2018-04-24 05:45:30', '2018-05-02 12:34:32'),
(17, 'hộp bút tự làm', 'hop-but-tu-lam', 'hộp bút', '<p>hộp bút tự làm&nbsp;</p>', 'sBHR_hop-dung-do-trang-diem-handmade.jpg', 1, 0, 10, '2018-04-24 05:51:46', '2018-05-02 12:42:44'),
(19, 'Dây Điện', 'day-dien', 'Các loại dây điện', '<p>Nhiều loại dây điện</p>\r\n\r\n<p>Sản xuất từ Trung Quốc và Nhật Bản</p>', 'iEMY_lua-chon-day-dien-phu-hop-cho-he-thong-dien-gia-dinh.jpg', 1, 0, 1, '2018-05-02 12:00:48', '2018-05-02 12:00:48'),
(20, 'Ổ Cắm', 'o-cam', 'Ổ Cắm', '<p>Được xuất xứ từ Nhật Bản</p>', 'z0Wg_o-cam-3-lo-day-2m-dien-quang-002a-02-1-700x467-1.jpg', 1, 0, 11, '2018-05-02 12:03:38', '2018-05-02 12:03:38'),
(22, 'Đèn LED', 'den-led', 'Combo 3 đèn LED dán tường loại 4 bóng', '<h2 style=\"text-align:justify\">Combo 3 đèn LED dán tường loại 4 bóng có thể dán lên bất cứ vị trí nào trong nhà là giải pháp chiếu sáng hiệu quả những lúc mất điện hoặc những khu vực thiếu ánh sáng. Chỉ 59.000đ cho trị giá 110.000đ.</h2>', '3NoX_0635475849019931383.jpg', 1, 0, 17, '2018-05-02 12:08:40', '2018-05-02 12:09:15'),
(23, 'Đèn LED 12W', 'den-led-12w', 'Đèn LED không cần điện 12W', '<h2 style=\"text-align:justify\">Bạn sẽ không còn phải lo tình trạng mất điện đột ngột vào những đêm tối trời nhờ những tính năng đặc biệt của đèn LED không cần điện 12W, đặc biệt là tính năng có thể xác định khi nào người dùng tắt điện bằng công tắc và khi nào điện bị cắt.</h2>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div class=\"detail_bar\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; clear: both; color: rgb(65, 65, 65); font-family: OpenSans, arial;\">&nbsp;</div>', 'rNGQ_0635981499719769312.jpg', 1, 0, 17, '2018-05-02 12:15:04', '2018-05-02 12:15:04'),
(24, 'Đèn pin LED 7', 'den-pin-led-7', 'Đèn pin LED 7 bóng đeo trán Light Lamp', '<h2 style=\"text-align:justify\">Đèn pin LED 7 bóng đeo trán Light Lamp với kiểu dáng gọn nhẹ, thiết kế thuận tiện sử dụng giúp bạn rảnh tay khi làm việc trong bóng tối, khi đi dã ngoại... Chỉ 45.000đ cho trị giá 90.000đ.</h2>', 'USmT_0635619305348189074.jpg', 1, 0, 19, '2018-05-02 12:16:47', '2018-05-02 12:16:47'),
(25, 'Bàng khô', 'bang-kho', 'Củ Ngưu Bàng khô - Nhân sâm của người Nhật gói 500gr', '<h2 style=\"text-align:justify\">Nho khô nhập khẩu Mỹ không chất bảo quản (500g) có hương vị ngọt dịu, dễ ăn, lại chứa nhiều dưỡng chất tốt cho sức khỏe, tiện dụng mang theo như loại thức ăn nhẹ, bổ dưỡng cho những người làm việc văn phòng, du lịch …</h2>', 'DEiI_300776_636516210409799712.jpg', 1, 0, 20, '2018-05-02 12:21:57', '2018-05-02 12:21:57'),
(26, 'Khô bò', 'kho-bo', 'Khô bò miếng mềm, món ngon chuẩn đón Tết (500gr)', '<h2 style=\"text-align:justify\">Cho ngày Xuân rộn ràng, ngày Tết trọn vẹn hơn khi cùng bạn bè, người thân nhâm nhi hương vị đậm đà, thơm ngon vị Khô bò miếng mềm, món ngon chuẩn đón Tết (500gr). Chỉ với 180.000đ cho trị giá 270.000đ</h2>', 'tRCe_305182_636591235581567459.jpg', 1, 0, 21, '2018-05-02 12:22:50', '2018-05-02 12:22:50'),
(27, 'Nho khô', 'nho-kho', 'Nho khô nhập khẩu Mỹ không chất bảo quản (500g)', '<h2 style=\"text-align:justify\">Nho khô nhập khẩu Mỹ không chất bảo quản (500g) có hương vị ngọt dịu, dễ ăn, lại chứa nhiều dưỡng chất tốt cho sức khỏe, tiện dụng mang theo như loại thức ăn nhẹ, bổ dưỡng cho những người làm việc văn phòng, du lịch …</h2>', 'aVD7_298587_636493788131796071.jpg', 1, 0, 22, '2018-05-02 12:23:44', '2018-05-02 12:23:44'),
(28, 'Khô heo', 'kho-heo', 'Khô heo giả bò chua cay, nguyên liệu sạch (500gr)', '<h2 style=\"text-align:justify\">Cho ngày Xuân rộn ràng, ngày Tết trọn vẹn hơn khi cùng bạn bè, người thân nhâm nhi hương vị đậm đà, thơm ngon vị Khô heo giả bò chua cay được làm từ nguyên liệu sạch (500gr) . Chỉ với 150.000đ cho trị giá 215.000đ.</h2>', 'h7ld_304993_636588711041925340.jpg', 1, 0, 23, '2018-05-02 12:24:46', '2018-05-02 12:24:46'),
(29, 'Nệm thú bông KITTY', 'nem-thu-bong-kitty', 'Nệm thú bông KITTY cao cấp Nhật Bản có túi ngủ', '<h2 style=\"text-align:justify\">Nệm thú bông KITTY cao cấp Nhật Bản có túi ngủ với thiết kế ngộ nghĩnh, đáng yêu, được làm từ chất liệu cao cấp, công nghệ chống xẹp cho bạn những giây phút thư giãn thoải mái nhất trong mọi tư thế.</h2>', 'DbyV_cm_b142697.jpg', 1, 0, 24, '2018-05-02 12:29:32', '2018-05-02 12:29:32'),
(30, 'Nệm thú bông Doremon', 'nem-thu-bong-doremon', 'Nệm thú bông Doremon cao cấp Nhật Bản', '<h2 style=\"text-align:justify\">Tận hưởng cảm giác thư giãn tối đa với nệm thú bông Doremon cao cấp Nhật Bản. Sản phẩm có thiết kế đáng yêu, chất liệu mềm mại, thoáng mát, tạo sự thoải mái trong mọi tư thế.</h2>', 'DFKK_0636316681472015186.jpg', 1, 0, 24, '2018-05-02 12:30:21', '2018-05-02 12:30:21'),
(31, 'Nệm thú bông', 'nem-thu-bong', 'Nệm thú bông Beary cao cấp Nhật Bản', '<h2 style=\"text-align:justify\">Tận hưởng cảm giác thư giãn với nệm thú bông Beary cao cấp Nhật Bản, được sản xuất từ chất liệu mềm mại, thoáng mát, thiết kế đáng yêu, cho bạn những giấc ngủ an nhiên, thư thái.</h2>', 'Ucwe_0636316752354483685.jpg', 1, 0, 24, '2018-05-02 12:31:51', '2018-05-02 12:31:51'),
(32, 'Tinh chất', 'tinh-chat', 'Tinh chất hàu tươi Orihiro Nhật Bản', '<h2 style=\"text-align:justify\">Tinh chất hàu tươi Orihiro là trợ thủ đắc lực giúp tăng cường sinh lý nam giới, tăng số lượng và cải thiện chất lượng tinh trùng. Tăng số lượng và cải thiện chất lượng tinh trùng, giảm tỷ lệ dị tật tinh trùng, tăng khả năng thụ thai.</h2>', 'yD0i_0636535138271398490.jpg', 1, 0, 25, '2018-05-02 12:33:21', '2018-05-02 12:33:21'),
(33, 'Mì dan dan', 'mi-dan-dan', 'Mì dan dan Tứ Xuyên', '<p><em>Đặc sản này gồm mì, chan sốt tương đen cay, cho thêm thịt lợn xay, tỏi xay, đậu phộng, hành tươi và rau mùi. Sốt được cho xuống dưới mì và thực khách cần trộn lên khi ăn</em></p>', 'VXEw_20170727102304-trung-quoc2.jpg', 1, 0, 26, '2018-05-02 12:38:29', '2018-05-02 12:38:29'),
(34, 'Bánh Bao', 'banh-bao', 'Bánh Bao', '<p>Bánh Bao</p>', 'bgVM_images867088_t3.jpg', 1, 0, 27, '2018-05-02 12:39:09', '2018-05-02 12:39:09'),
(35, 'Miến chua ngọt', 'mien-chua-ngot', 'Miến chua ngọt Trùng Khánh', '<p><em>Món ăn đường phố này có giá khá rẻ và phổ biến khắp Trung Quốc. Miến làm từ bột khoai lang được nấu trong nước dùng làm từ đậu nành, tương ớt, dấm và sa tế.</em></p>', 'Yx4u_20170727102858-trung-quoc4.jpg', 1, 0, 28, '2018-05-02 12:40:31', '2018-05-02 12:40:31'),
(36, 'Cá hộp', 'ca-hop', '3 cô Gái', '<p>3 cô Gái</p>', '4Hzb_timthumb.jpg', 1, 0, 14, '2018-05-02 12:44:29', '2018-05-02 12:44:29'),
(37, 'Cá Ngừ Cá Hộp', 'ca-ngu-ca-hop', 'Cá Ngừ Cá Hộp', '<p>Cá Ngừ Cá Hộp</p>', 'b8JV_tải xuống.jpg', 1, 0, 14, '2018-05-02 12:45:31', '2018-05-02 12:45:31'),
(38, 'Kem Hàn Quốc', 'kem-han-quoc', 'Kem Hàn Quốc', '<p>Kem Hàn Quốc</p>', 'bMV4_foody-mobile-t1-jpg-778-635998790839200009.jpg', 1, 0, 7, '2018-05-02 12:48:07', '2018-05-02 12:48:07'),
(39, 'Bánh kem', 'banh-kem', 'Bánh kem bắp dâu tây sấy (16 x 6 cm) - Bánh Kem Thiên Thần', '<h2 style=\"text-align:justify\">Hãy chia sẻ hương vị ngọt ngào tuyệt ngon cùng người thân, bạn bè với bánh kem bắp dâu tây sấy (16 x 6 cm) tại Bánh Kem Thiên Thần. Chỉ 99.000đ cho trị giá 198.000đ.</h2>', 'hSdq_301674_636525612364086626.jpg', 1, 0, 29, '2018-05-02 12:48:55', '2018-05-02 12:48:55'),
(40, 'son', 'son', 'son louboutin hạ giá', '<p>giá 2.5tr&nbsp;</p>', 'KDoo_0-cc942.jpg', 1, 0, 30, '2018-05-07 01:00:10', '2018-05-07 01:00:10'),
(41, 'Áo Trẻ Con', 'ao-tre-con', 'Áo Trẻ Em', '<p>giảm giá 50%</p>', 'LQ68_quan-ao-so-sinh-1.jpg', 1, 0, 31, '2018-05-07 02:26:46', '2018-05-07 02:27:18'),
(42, 'fhghgh', 'fhghgh', 'gfhgfhgf', '<p style=\"text-align: center;\"><strong>thứ tự</strong></p>\r\n\r\n<ol>\r\n	<li><span style=\"color:#00FF00\"><strong>a</strong></span></li>\r\n	<li><span style=\"color:#00FF00\"><strong>2</strong></span></li>\r\n	<li><strong>v</strong></li>\r\n	<li><strong>2</strong></li>\r\n	<li><strong>4</strong></li>\r\n</ol>\r\n\r\n<p><a href=\"https://www.google.com.vn/url?sa=i&amp;rct=j&amp;q=&amp;esrc=s&amp;source=images&amp;cd=&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjTibDg79zdAhVHXLwKHW9nDVAQjRx6BAgBEAU&amp;url=https%3A%2F%2Fpixabay.com%2Fen%2Fimage-statue-brass-child-art-1465348%2F&amp;psig=AOvVaw3rTcT55rTEeJooJLoyLFWo&amp;ust=1538195722713663\"><img alt=\"\" src=\"https://www.google.com.vn/url?sa=i&amp;rct=j&amp;q=&amp;esrc=s&amp;source=images&amp;cd=&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjTibDg79zdAhVHXLwKHW9nDVAQjRx6BAgBEAU&amp;url=https%3A%2F%2Fpixabay.com%2Fen%2Fimage-statue-brass-child-art-1465348%2F&amp;psig=AOvVaw3rTcT55rTEeJooJLoyLFWo&amp;ust=1538195722713663\" /></a></p>', 'cYZ9_a1.jpg', 1, 0, 1, '2018-09-27 21:45:27', '2018-09-27 21:45:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quyen` int(11) NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `quyen`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn\r\n Đình Chiến', 'nguyendinhchien15011996@gmail.com', 1, '$2y$10$NN9WLqHGJrentVqVfYQqGOaAGllHzuwETwb/WGGQ1zlR9C4RTsFd.', 'nL1RY688IM5gOrpLc87dkdHCGBH828flEEJzzd5GytEtlgpLRxkBVa1XmMwC', NULL, '2018-04-24 04:44:28'),
(2, 'Lê Thị Oanh', 'oanhoneone@gmail.com', 1, '$2y$10$gynWtLg7m0MuQS1N5eyFc.KYdxYWeWTqt6v5zPKVnrPfmq47NGd.W', 'SeogC4o1ijUZKwaV227OiFuHIUXXT96kKhE3hVurt5yTjrZK1r7xui3ArDoK', '2018-04-24 04:57:34', '2018-04-26 09:56:58'),
(4, 'Lê Thị Cúc', 'cucdtvt.bkhn@gmail.com ', 1, '$2y$10$0GGyEYhZSrMpYQK5gA/H7ujvMGPPtmLgWCPNn.FtFjIpXELyXBZT2', 'zXNQsjq2zBVMSCUZAiCcCBYEsXOZglRGD634XCzpWS7KAJtvYsLGD6FN9hIw', '2018-04-24 05:53:51', '2018-04-24 05:53:51'),
(5, 'Đinh Thế Công', 'dinhthecong@outlook.com', 1, '$2y$10$Sx/HlBs5.f9v4.Da43ANcuqBVMfb4baLbW3EHplyxiw2eQUG5Kf9u', 'H7veCrkqDKwwIZw0DZV4uT657ZMB1CkD8t2kqfhZNsLnpDCZsoHaL4MrrD8m', '2018-04-26 09:50:22', '2018-04-26 09:50:22'),
(6, 'Tạ Thị Phượng', 'phuongta1996@gmail.com', 1, '$2y$10$ZhoTJP0IvEYqI0E.9L24cOhQ9uwWghBQludUUxoQ4mrRjqvw2Odgm', NULL, '2018-05-02 11:24:58', '2018-05-02 11:24:58'),
(7, 'Trần Văn Tâm', 'tranvantam74k17c@gmail.com', 1, '$2y$10$rk7bJZwKjiZDiufzn96dgO16SsGmQCJZ1Nl32MIXff9FrT.GCRfBG', NULL, '2018-05-02 11:31:55', '2018-05-02 11:31:55'),
(8, 'Nguyễn Thị Lệ', 'lenguyenhd96@gmail.com', 1, '$2y$10$k6EB6BK6c33vPUAiSkzXju4bbIrubNhcS2tkPqCJu81641KooYylC', NULL, '2018-05-02 11:27:01', '2018-05-02 11:27:01'),
(9, 'Đỗ Thị Thảo', 'do.thao747@gmail.com', 1, '$2y$10$gmwpYMGg96zDpZKWFf8Vwua6wrXZ6cpb9rD3NJL6vDKbombKIMQ3K', NULL, '2018-05-02 11:33:42', '2018-05-02 11:33:42'),
(10, 'Nguyễn Thị Giang', 'nguyenthigiang@gmail.com', 1, '$2y$10$KIzYkgP2yJIp4xa/yiC/reEauuZFxGP9ASNoZp4qYC4sJvQXg32Qu', NULL, '2018-05-02 11:36:52', '2018-05-02 11:36:52'),
(11, 'Phương Lan', 'lanlan@gmail.com', 0, '$2y$10$zEfYSX4y3h8LtEhPKUkiAeF64jOlNn.fdr2KHj5BD3g6.o20PG19W', NULL, '2018-04-23 11:26:32', '2018-04-23 11:26:32'),
(12, 'người dùng', 'nguoidung@gmail.com', 0, '$2y$10$E6XNNxQq0udJRneNa4cAH.tAojUjh1qNdO0NsegOmHqtzU7xnNC.e', '6FiQ2ay00YNFa1BEuqZczlowK6MVDBs7sPNZgJ01ZyE7txoJln3qYjkT7zMT', '2018-05-02 21:39:28', '2018-05-02 21:39:28'),
(13, 'Nguyễn Thị Quỳnh', 'quynhnguyen@gmail.com', 0, '$2y$10$sjYiOFkq/u2Pi3E7m6P0QebUtev6pjWmrAG3Y4jTNq.dyfzwW57Rm', NULL, '2018-05-03 11:02:19', '2018-05-03 11:02:19'),
(14, 'không tên', 'khongbiet@gmail.com', 0, '$2y$10$C46FFJoQuHReE0JAFMZ8fub3z9CYzW/4kVV/vBpEEtVuz.JmS7QxO', 'itjLpbBNsDBEpyS9WxhT5pqmG7yO7837Ag7dL2GoCL6cVWAfdNMV0bPG3j5v', '2018-05-07 00:57:06', '2018-05-07 00:57:06'),
(15, 'oanh phương', 'oanhphuong@gmail.com', 0, '$2y$10$PAOvmzQJ5P2DVEyRCr5/8OcCCl39fu42YyOKK/7OwLEwlpSaA.fxG', 'JHB3LSwYKIolw8GlXROIFHVWjYmend7LAY17r1IbSLe7AJgT8if2F1x3D1XU', '2018-05-07 01:00:40', '2018-05-07 01:00:40'),
(16, 'Nguyễn', 'nguyen@gmail.com', 0, '$2y$10$skCROIJpnfQaQU7rzgS9OOV.RgfYVHav24Y.CcqcBzQsUFXFZgvJy', 'fNd8ps7WmUWSj4ZtPYmOyCDME6V1adCht4BJOgRPT0ncD4hD6FyCUSPrl4TG', '2018-05-07 01:10:13', '2018-05-07 01:10:13'),
(17, 'Lê Thị Phương', 'lethiphuong20101996@gmail.com', 1, '$2y$10$71JjmTERVM5GebkuFQzOneDSMHdBt.sNNCRCZfDXiPIORK/aVwtx6', 'e56ad3MzygDgaKol0kgppXNlbAu3xkCA5RQxKF4AL9a1MqaDECg68cqSAshm', '2018-05-07 02:21:40', '2018-05-07 02:21:40');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_iduser_foreign` (`iduser`),
  ADD KEY `comment_idtintuc_foreign` (`idtintuc`);

--
-- Chỉ mục cho bảng `ketquado`
--
ALTER TABLE `ketquado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ketquado_idthongtindiemdo_foreign` (`idthongtindiemdo`);

--
-- Chỉ mục cho bảng `loaisanpham`
--
ALTER TABLE `loaisanpham`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loaisanpham_idsanpham_foreign` (`idsanpham`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `thongtindiemdo`
--
ALTER TABLE `thongtindiemdo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thongtindiemdo_idtinhtrang_foreign` (`idtinhtrang`);

--
-- Chỉ mục cho bảng `tinhtrang`
--
ALTER TABLE `tinhtrang`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tintuc_idloaisanpham_foreign` (`idloaisanpham`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `ketquado`
--
ALTER TABLE `ketquado`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `loaisanpham`
--
ALTER TABLE `loaisanpham`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `thongtindiemdo`
--
ALTER TABLE `thongtindiemdo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `tinhtrang`
--
ALTER TABLE `tinhtrang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_idtintuc_foreign` FOREIGN KEY (`idtintuc`) REFERENCES `tintuc` (`id`),
  ADD CONSTRAINT `comment_iduser_foreign` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `ketquado`
--
ALTER TABLE `ketquado`
  ADD CONSTRAINT `ketquado_idthongtindiemdo_foreign` FOREIGN KEY (`idthongtindiemdo`) REFERENCES `thongtindiemdo` (`id`);

--
-- Các ràng buộc cho bảng `loaisanpham`
--
ALTER TABLE `loaisanpham`
  ADD CONSTRAINT `loaisanpham_idsanpham_foreign` FOREIGN KEY (`idsanpham`) REFERENCES `sanpham` (`id`);

--
-- Các ràng buộc cho bảng `thongtindiemdo`
--
ALTER TABLE `thongtindiemdo`
  ADD CONSTRAINT `thongtindiemdo_idtinhtrang_foreign` FOREIGN KEY (`idtinhtrang`) REFERENCES `tinhtrang` (`id`);

--
-- Các ràng buộc cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD CONSTRAINT `tintuc_idloaisanpham_foreign` FOREIGN KEY (`idloaisanpham`) REFERENCES `loaisanpham` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
