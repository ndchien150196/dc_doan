<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersSeeder');
    }
}
class UsersSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('users')-> insert([
        		['name'=>'Chien','email'=>'nguyendinhchien15011996@gmail.com','password' => bcrypt('nguyendinhchien'),
	            	'quyen'=> 0,],
        		
        ]);
    }
}