<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKetquadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ketquado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nhietdo');
            $table->string('doam');
            $table->string('anhsang');
            $table->string('co');
            $table->string('buipm1');
            $table->string('buipm25');
            $table->string('pin');
            $table->string('mota');
            $table->integer('idthongtindiemdo')->unsigned();
            $table->foreign('idthongtindiemdo')->references('id')->on('thongtindiemdo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ketquado');
    }
}
