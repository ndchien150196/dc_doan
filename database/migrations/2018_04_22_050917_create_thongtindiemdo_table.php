<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThongtindiemdoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thongtindiemdo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tendiemdo');
            $table->integer('idtinhtrang')->unsigned();
            $table->foreign('idtinhtrang')->references('id')->on('tinhtrang');
            $table->string('mota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thongtindiemdo');
    }
}
