@extends('layout.index')
@section('content')

<!-- Page Content -->
    <div class="container">

    	@include('layout.slide')

        <div class="space20"></div>


        <div class="row main-left">
             @include('layout.menu')

            <div class="col-md-9">
	            <div class="panel panel-default">            
	            	<div class="panel-heading" style="background-color:#337AB7; color:white;" >
	            		<h2 style="margin-top:0px; margin-bottom:0px;">Tin Tức</h2>
	            	</div>

	            	<div class="panel-body">
	            		@foreach($sanpham as $sp)
	            			@if(count($sp->loaisanpham)>0)
	            		<!-- item -->
					   		  <div class="row-item row">
		                	<h3>
		                		<a href="category.html">{{$sp->Ten}}</a> |
		                		@foreach($sp->loaisanpham as $lsp) 	
		                		<small>
		                			<a href="loaisanpham/{{$lsp->id}}/{{$lsp->TenKhongDau}}.html"><i>{{$lsp->Ten}}</i></a>/</small>
		                		@endforeach
		                	</h3>
		                	<?php
		                	$data= $sp->tintuc->where('NoiBat',1)->sortByDesc('create_at')->take(5);
		                	$tin1=$data->shift();

		                	?>
		                	<div class="col-md-8 border-right">
		                		<div class="col-md-5">
			                        <a href="tintuc/{{$tin1['id']}}/{{$tin1['TieuDeKhongDau']}}.html">
			                            <img class="img-responsive" src="upload/tintuc/{{$tin1['Hinh']}}" alt="">
			                        </a>
			                    </div>

			                    <div class="col-md-7">
			                        <h3>{{$tin1['TomTat']}}</h3>
			                        <p>{{$tin1['TieuDe']}}</p>
			                        <a class="btn btn-primary" href="tintuc/{{$tin1['id']}}/{{$tin1['TieuDeKhongDau']}}.html">Xem Thêm <span class="glyphicon glyphicon-chevron-right"></span></a>
								</div>

		                	</div>
		                    

							<div class="col-md-4">
								@foreach($data->all() as $tintuc)
								<a href="tintuc/{{$tintuc['id']}}/{{$tintuc['TieuDeKhongDau']}}.html">
									<h4>
										<span class="glyphicon glyphicon-list-alt"></span>
										{{$tintuc['TieuDe']}}
									</h4>
								</a>

								@endforeach
							</div>
							
							<div class="break"></div>
		              		  </div>
		                <!-- end item -->
		                	@endif
		                @endforeach
					</div>
	            </div>
        	</div>
        </div>
        <!-- /.row -->
    </div>
    <!-- end Page Content -->
@endsection