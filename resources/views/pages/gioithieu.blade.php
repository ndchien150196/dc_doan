@extends('layout.index')
@section('content')
<h1 style="text-align: center">Lịch Sử</h1>
<h3 style="text-indent">
	Phần lớn không gian được dành cho hàng tiêu dùng và thực phẩm với giá cả hợp lý và chất lượng được kiểm soát. Sản phẩm kinh doanh có thể được chia ra thành 5 ngành chính, như sau:<ul>
	<li>Thực phẩm tươi sống: thịt gia súc, gia cầm, thủy hải sản, trái cây và rau củ, thực phẩm chế biến, thực phẩm đông lạnh, thực phẩm bơ sữa, bánh mì.</li>
	<li>Thực phẩm khô: Gia vị, nước giải khát, nước ngọt, rượu, bánh snack, hóa phẩm, mỹ phẩm, thực phẩm cho thú cưng và những phụ kiện.</li>
	<li>Hàng may mặc và phụ kiện: thời trang nam, nữ, trẻ em và trẻ sơ sinh, giày dép và túi xách.</li>
	<li>Hàng điện gia dụng: các sản phẩm điện gia dụng đa dạng bao gồm thiết bị trong nhà bếp, thiết bị giải trí tại gia, máy vi tính, các dụng cụ và các thiết bị tin học.</li>
	<li>Vật dụng trang trí nội thất: bàn ghế, dụng cụ bếp, đồ nhựa, đồ dùng trong nhà, những vật dụng trang trí, vật dụng nâng cấp, bảo trì và sửa chữa, phụ kiện di động, xe gắn máy, đồ dùng thể thao và đồ chơi.</li>
	</ul>
</h3>
<hr>
<h1 style="text-align: center"> CÁC TIỆN ÍCH KHI MUA SẮM TẠI <i>HOUK17BC SUPER CENTRER</i></h1>
<h3>
	<ul>
		<li>Bãi giữ xe miễn phí rộng rãi.</li>
		<li>Nhân viên tư vấn sản phẩm nhiệt tình.</li>
		<li>Giá rẻ như giá chợ.</li>
		<ul>Hoạt động kinh doanh tại các Hành lang có thể chia ra thành 4 nhóm chính:
			<li>Ăn – uống: nhà hàng, khu thức ăn nhanh, khu ẩm thực.</li>
			<li>Giải trí: rạp chiếu phim, quầy karaoke, và sân chơi dành cho thiếu nhi.</li>
			<li>Những cửa hàng khác: nhà sách, cửa hàng quần áo, cửa hàng điện thoại, điện tử.</li>
			<li>Dịch vụ: Máy rút tiền tự động (ATM)...</li>
		</ul>
	</ul>
</h3>
@endsection