 @extends('layout.index')
 @section('content') 
   <body>
<div style="width:80%; text-align: center">
<svg width="660" height="600" style="border:1px red solid">
        </svg>
</div>
<div >
      <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Tên Điểm Đo</th>
                                <th>Nhiệt Độ</th>
                                <th>Độ Ẩm</th>
                                <th>Ánh Sáng</th>
                                <th>CO</th>
                                <th>Bụi PM1</th>
                                <th>Bụi PM2.5</th>
                                <th>Pin</th>
                                <th>Mô Tả</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX" align="center">
                                <th><div id='tendiemdo'></div></th>
                                <th><div id='nhietdo'></div></th>
                                <th><div id='doam'></div></th>
                                <th><div id='anhsang'></div></th>
                                <th><div id='co'></div></th>
                                <th><div id='buipm1'></div></th>
                                <th><div id='buipm25'></div></th>
                                <th><div id='pin'></div></th>
                                <th><div id='mota'></div></th>
                                <th><div id='time'></div></th>
                            </tr>
                        </tbody>
                    </table>
</div>
    @foreach($ketquado as $kqd)
    <script src="http://d3js.org/d3.v4.min.js"></script>
    <!-- .attrs() -->
    <script src="https://d3js.org/d3-selection-multi.v1.min.js"></script>
    <script>
	var dataset = [
    	[ 10,  10,'{"tendiemdo":"Hàng Trung", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "34 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:20"}'],
    	[ 130,  10,'{"tendiemdo":"Hàng Trung", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "34 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:20"}'],
        [ 260,  10,'{"tendiemdo":"Hàng Nhật", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "34 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:30"}'],
        [ 390,  10,'{"tendiemdo":"Hàng Nhật", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "34 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:30"}'],
        [ 520,  10,'{"tendiemdo":"Hàng Hàn", "temp":"26 *C","humidity":"20 %","light":"60 lux","gas_co": "37 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:35"}'],
        [ 630,  10,'{"tendiemdo":"Hàng Hàn", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "37 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:35"}'],
        
        [ 10,  160,'{"tendiemdo":"Hàng Khô", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "35 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:15"}'],
        [ 130,  160,'{"tendiemdo":"Hàng Khô", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "35 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:15"}'],
        [ 260,  160,'{"tendiemdo":"Hàng Hộp", "temp": "22 *C","humidity":"20 %","light":"60 lux","gas_co": "32ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:30"}'],
        [ 390,  160,'{"tendiemdo":"Hàng Hộp", "temp": "22 *C","humidity":"20 %","light":"60 lux","gas_co": "32 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:30"}'],
        [ 520,  160,'{"tendiemdo":"Hàng Điện", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "34 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:40"}'],
        [ 630,  160,'{"tendiemdo":"Hàng Điện", "temp": "26 *C","humidity":"20 %","light":"60 lux","gas_co": "34 ppb","gas_dust1" : "50 pm","gas_dust25" : "30 pm","eng" : "90 %","acti" : "An Toàn","time":"2018-05-02 20:27:40"}'],
        
	
        [ 35,  480,'{ "tendiemdo":"{{$kqd->thongtindiemdo->tendiemdo}}","temp": "{{$kqd->nhietdo}}","humidity":"{{$kqd->doam}}","light":"{{$kqd->anhsang}}","gas_co": "{{$kqd->co}}","gas_dust1" : "{{$kqd->buipm1}}","gas_dust25" : "{{$kqd->buipm25}}","eng" : "{{$kqd->pin}}","acti" : "{{$kqd->mota}}","time":"{{$kqd->updated_at}}"}'],
        [ 130,  360,'{ "tendiemdo":"{{$kqd->thongtindiemdo->tendiemdo}}","temp": "{{$kqd->nhietdo}}","humidity":"{{$kqd->doam}}","light":"{{$kqd->anhsang}}","gas_co": "{{$kqd->co}}","gas_dust1" : "{{$kqd->buipm1}}","gas_dust25" : "{{$kqd->buipm25}}","eng" : "{{$kqd->pin}}","acti" : "{{$kqd->mota}}","time":"{{$kqd->updated_at}}"}'],
        [ 290,  480,'{ "tendiemdo":"{{$kqd->thongtindiemdo->tendiemdo}}","temp": "{{$kqd->nhietdo}}","humidity":"{{$kqd->doam}}","light":"{{$kqd->anhsang}}","gas_co": "{{$kqd->co}}","gas_dust1" : "{{$kqd->buipm1}}","gas_dust25" : "{{$kqd->buipm25}}","eng" : "{{$kqd->pin}}","acti" : "{{$kqd->mota}}","time":"{{$kqd->updated_at}}"}'],
        [ 390,  360,'{ "tendiemdo":"{{$kqd->thongtindiemdo->tendiemdo}}","temp": "{{$kqd->nhietdo}}","humidity":"{{$kqd->doam}}","light":"{{$kqd->anhsang}}","gas_co": "{{$kqd->co}}","gas_dust1" : "{{$kqd->buipm1}}","gas_dust25" : "{{$kqd->buipm25}}","eng" : "{{$kqd->pin}}","acti" : "{{$kqd->mota}}","time":"{{$kqd->updated_at}}"}'],
        [ 580,  480,'{ "tendiemdo":"{{$kqd->thongtindiemdo->tendiemdo}}","temp": "{{$kqd->nhietdo}}","humidity":"{{$kqd->doam}}","light":"{{$kqd->anhsang}}","gas_co": "{{$kqd->co}}","gas_dust1" : "{{$kqd->buipm1}}","gas_dust25" : "{{$kqd->buipm25}}","eng" : "{{$kqd->pin}}","acti" : "{{$kqd->mota}}","time":"{{$kqd->updated_at}}"}'],
        [ 630,  360,'{ "tendiemdo":"{{$kqd->thongtindiemdo->tendiemdo}}","temp": "{{$kqd->nhietdo}}","humidity":"{{$kqd->doam}}","light":"{{$kqd->anhsang}}","gas_co": "{{$kqd->co}}","gas_dust1" : "{{$kqd->buipm1}}","gas_dust25" : "{{$kqd->buipm25}}","eng" : "{{$kqd->pin}}","acti" : "{{$kqd->mota}}","time":"{{$kqd->updated_at}}"}'],
        
	
      ];

        var svg = d3.select('svg')
                    .append('svg')
                    .attrs({width: 800, height: 600});

	var i=0;
	for(i=0;i<dataset.length;i++){
		controlData(dataset[i][0],dataset[i][1],20,100,'green',i);
	}


	function controlData(x1,y1,with1,length1,color1,point){

		svg.append('rect')
           	.attrs({ x:x1,  y:y1,m:point, width: with1, height: length1, fill: color1})
		.on("click", function(){
			//alert(color1 +x1.toString());
			var i=0;
			for(i=0;i<dataset.length;i++){
				if((dataset[i][0]==x1)&&(dataset[i][1]==y1))
				{
					var student_obj = JSON.parse(dataset[i][2].toString());
                    document.getElementById("tendiemdo").innerHTML = student_obj.tendiemdo;
					document.getElementById("nhietdo").innerHTML = student_obj.temp;
					document.getElementById("doam").innerHTML = student_obj.humidity;
                    document.getElementById("anhsang").innerHTML = student_obj.light;
					document.getElementById("co").innerHTML = student_obj.gas_co;
                    document.getElementById("buipm1").innerHTML = student_obj.gas_dust1;
                    document.getElementById("buipm25").innerHTML = student_obj.gas_dust25;
                    document.getElementById("pin").innerHTML = student_obj.eng;
                    document.getElementById("mota").innerHTML = student_obj.acti;
                    document.getElementById("time").innerHTML = student_obj.time;
				}
				
			}
			
		})
		.text("Blue Line");

	}
 
    </script>
    @endforeach
</body>

<body>

<p>Click the button to display the time.</p>

<button onclick="getElementById('demo').innerHTML=Date()">What is the time?</button>

<p id="demo"></p>
</body>

<body>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
#myDIV {
    width: 100%;
    padding: 50px 0;
    text-align: center;
    background-color: lightblue;
    margin-top: 20px;
}
</style>
</head>





@endsection