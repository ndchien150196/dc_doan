@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tin Tức
                            <small>{{$tintuc->TieuDe}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) >0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{$err}}<br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                
                                {{session('thongbao')}}
                                
                            </div>
                        @endif
                        <form action="admin/tintuc/sua/{{$tintuc->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div class="form-group">
                                <label>Sản Phẩm</label>
                                <select class="form-control" name="sanpham" id="sanpham">
                                    @foreach($sanpham as $sp)
                                    <option 
                                    @if($tintuc->loaisanpham->sanpham->id==$sp->id)
                                    {{"selected"}}
                                    @endif



                                    value="{{$sp->id}}">{{$sp->Ten}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                             <div class="form-group">
                                <label>Loại Sản Phẩm</label>
                                <select class="form-control" name="loaisanpham" id="loaisanpham">
                                    @foreach($loaisanpham as $lsp)
                                    <option 

                                    @if($tintuc->loaisanpham->id==$lsp->id)
                                    {{"selected"}}
                                    @endif


                                    value="{{$lsp->id}}">{{$lsp->Ten}}</option>
                                    @endforeach
                                 </select>
                            </div>
                            <div class="form-group">
                                <label>Tiêu Đề</label>
                                <input class="form-control" name="TieuDe" placeholder="Nhập Tiêu Đề" value="{{$tintuc->TieuDe}}" />
                            </div>
                            
                            <div class="form-group">
                                <label>Tóm Tắt</label>
                                <input class="form-control" name="TomTat" id="demo" class="form-control ckeditor" placeholder="Nhập Tóm Tắt" value="{{$tintuc->TomTat}}" />
                                
                            </div>
                            <div class="form-group">
                                <label>Nội Dung</label>
                                <textarea id="demo" name="NoiDung" class="form-control ckeditor" rows="5">
                                    {{$tintuc->NoiDung}}
                                </textarea>
                            </div>

                            <div class="form-group">
                                <label>Hình Ảnh</label>
                                <p>
                                <img width="400pm" src="upload/tintuc/{{$tintuc->Hinh}}">
                                 </p>
                                <input type="file" name="Hinh">
                            </div>


                            <div class="form-group">
                                <label>Nổi Bật</label>
                                <label class="radio-inline">
                                    <input name="NoiBat" value="0" 
                                    @if($tintuc->NoiBat ==0) 
                                    {{"checked"}}
                                    @endif
                                     type="radio">Không
                                </label>
                                <label class="radio-inline">
                                    <input name="NoiBat" value="1" 
                                    @if($tintuc->NoiBat == 1) 
                                    {{"checked"}}
                                    @endif
                                    type="radio">Có
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm Mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->


                 <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Bình Luận
                            <small>Danh Sách</small>
                        </h1>
                    </div>
                    

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                
                                {{session('thongbao')}}
                                
                            </div>
                        @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Người Dùng</th>
                                <th>Nội Dung</th>
                                <th>Ngày Đăng</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tintuc -> comment as $cm)
                            <tr class="odd gradeX" align="center">
                                <td>{{$cm->id}}</td>
                                <td>cm{{$cm->user->name}}</td>
                                <td>{{$cm->NoiDung}}</td>
                                <td>{{$cm->created_at}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/comment/xoa/{{$cm->id}}/{{$tintuc->id}}"> Delete</a></td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>



                <!--/end row -->








            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->





@endsection
@section('script')
    <script >
        $(document).ready(function(){
            $('#sanpham').change(function(){
                var idsanpham =$(this).val();
                $.get("admin/ajax/loaisanpham/"+idsanpham,function(data){
                    $("#loaisanpham").html(data);
                });
            });
        });
    </script>
@endsection('script')
