@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tình Trạng
                            <small>Vị Trí:{{$tinhtrang->id}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) >0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{$err}}<br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                
                                {{session('thongbao')}}
                                
                            </div>
                        @endif
                        <form action=""admin/tinhtrang/sua/{{$tinhtrang->id}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />

                            <div class="form-group">
                                <label>DHT11:</label> <br>
                                <label class="radio-inline">
                                    <input name="dht11" value="0" checked="" type="radio">Hỏng
                                </label>
                                <label class="radio-inline">
                                    <input name="dht11" value="1" checked="" type="radio">Hoạt Động
                                </label>
                                
                            </div>
                            <div class="form-group">
                                <label>SPEC Sensor:</label><br>
                                <label class="radio-inline">
                                    <input name="spec" value="0" checked="" type="radio">Hỏng
                                </label>
                                <label class="radio-inline">
                                    <input name="spec" value="1" checked="" type="radio">Hoạt Động
                                </label>
                                
                            </div>
                            <div class="form-group">
                                <label>DUST Sensor:</label><br>
                                <label class="radio-inline">
                                    <input name="dust" value="0" checked="" type="radio">Hỏng
                                </label>
                                <label class="radio-inline">
                                    <input name="dust" value="1" checked="" type="radio">Hoạt Động
                                </label>
                                
                            </div>
                            <div class="form-group">
                                <label>BH1750:</label><br>
                                <label class="radio-inline">
                                    <input name="bh1750" value="0" checked="" type="radio">Hỏng
                                </label>
                                <label class="radio-inline">
                                    <input name="bh1750" value="1" checked="" type="radio">Hoạt Động
                                </label>
                                
                            </div>
                            <div class="form-group">
                                <label>Mô Tả</label>
                                <input class="form-control" name="mota" value="{{$tinhtrang->mota}}" placeholder="Nhập Mô Tả" />
                            </div>
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm Mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection