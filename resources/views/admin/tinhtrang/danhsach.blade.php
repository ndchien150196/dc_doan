@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tình Trạng Thiết Bị
                            <small>Danh Sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>DHT11</th>
                                <th>SPEC SENSOR </th>
                                <th>DUST SENSOR </th>
                                <th>BH1750</th>
                                <th>MÔ TẢ</th>
                                <th>Time</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($tinhtrang as $tt)
                            <tr class="odd gradeX" align="center">
                                <td>{{$tt->id}}</td>
                                <td>@if($tt->dht11 ==1)
                                    {{"Hoạt Động"}}
                                    @else
                                    {{"Hỏng"}}
                                    @endif</td>
                                <td>@if($tt->spec ==1)
                                    {{"Hoạt Động"}}
                                    @else
                                    {{"Hỏng"}}
                                    @endif</td>
                                <td>@if($tt->dust ==1)
                                    {{"Hoạt Động"}}
                                    @else
                                    {{"Hỏng"}}
                                    @endif</td>
                                <td>@if($tt->bh1750 ==1)
                                    {{"Hoạt Động"}}
                                    @else
                                    {{"Hỏng"}}
                                    @endif</td>
                                <td>{{$tt->mota}}</td>
                                <td>{{$tt->updated_at}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/tinhtrang/xoa/{{$tt->id}}">Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/tinhtrang/sua/{{$tt->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                         
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection