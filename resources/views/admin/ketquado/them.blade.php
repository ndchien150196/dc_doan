@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Kết Qủa Đo
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) >0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{$err}}<br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                
                                {{session('thongbao')}}
                                
                            </div>
                        @endif
                        <form action="admin/ketquado/them" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div class="form-group">
                                <label>Tên Điểm Đo</label>
                                <select class="form-control" name="thongtindiemdo" id="thongtindiemdo">
                                    @foreach($thongtindiemdo as $ttd)
                                    <option value="{{$ttd->id}}">{{$ttd->tendiemdo}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nhiệt Độ</label>
                                <input class="form-control" name="nhietdo" placeholder="Nhập Nhiệt Độ" />
                            </div>
                            <div class="form-group">
                                <label>Độ Ẩm</label>
                                <input class="form-control" name="doam" placeholder="Nhập Độ Ẩm" />
                            </div>
                            <div class="form-group">
                                <label>Ánh Sáng</label>
                                <input class="form-control" name="anhsang" placeholder="Nhập Ánh Sáng" />
                            </div>
                            <div class="form-group">
                                <label>CO</label>
                                <input class="form-control" name="co" placeholder="Nhập CO" />
                            </div>
                            <div class="form-group">
                                <label>Bụi PM 1</label>
                                <input class="form-control" name="buipm1" placeholder="Nhập Bụi PM 1" />
                            </div>
                            <div class="form-group">
                                <label>Bụi PM 2.5</label>
                                <input class="form-control" name="buipm25" placeholder="Nhập Bụi PM 2.5" />
                            </div>
                            <div class="form-group">
                                <label>Pin</label>
                                <input class="form-control" name="pin" placeholder="Nhập Pin" />
                            </div>
                            <div class="form-group">
                                <label>Mô Tả</label>
                                <input class="form-control" name="mota" placeholder="Nhập Mô Tả" />
                            </div>
                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm Mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection