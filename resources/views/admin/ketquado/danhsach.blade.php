@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Kết Qủa Đo
                            <small>Danh Sách</small>
                        </h1>
                    </div>
                    

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                
                                {{session('thongbao')}}
                                
                            </div>
                        @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên Điểm Đo</th>
                                <th>Nhiệt Độ</th>
                                <th>Độ Ẩm</th>
                                <th>Ánh Sáng</th>
                                <th>CO</th>
                                <th>Bụi PM1</th>
                                <th>Bụi PM2.5</th>
                                <th>Pin</th>
                                <th>Mô Tả</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ketquado as $kqd)
                            <tr class="odd gradeX" align="center">
                                <td>{{$kqd->id}}</td>
                                <td>{{$kqd->thongtindiemdo->tendiemdo}}</td>
                                <td>{{$kqd->nhietdo}}</td>
                                <td>{{$kqd->doam}}</td>
                                <td>{{$kqd->anhsang}}</td>
                                <td>{{$kqd->co}}</td>
                                <td>{{$kqd->buipm1}}</td>
                                <td>{{$kqd->buipm25}}</td>
                                <td>{{$kqd->pin}}</td>
                                <td>{{$kqd->mota}}</td>                    
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/ketquado/xoa/{{$kqd->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/ketquado/sua/{{$kqd->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection