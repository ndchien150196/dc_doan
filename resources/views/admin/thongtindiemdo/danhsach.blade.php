@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thông Tin Điểm Đo
                            <small>Danh Sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên Điểm Đo</th>
                                <th>Vị Trí </th>
                                <th>Mô Tả</th>
                                <th>Time</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($thongtindiemdo as $ttd)  
                            <tr class="odd gradeX" align="center">
                                <td>{{$ttd->id}}</td>
                                <td>{{$ttd->tendiemdo}}</td>
                                <td>{{$ttd->tinhtrang->id}}</td>
                                <td>{{$ttd->mota}}</td>
                                <th>{{$ttd->updated_at}}</th>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/thongtindiemdo/xoa/{{$ttd->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/thongtindiemdo/sua/{{$ttd->id}}">Edit</a></td>
                            </tr>
                         @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection