@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thông Tin Điểm Đo
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) >0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{$err}}<br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                
                                {{session('thongbao')}}
                                
                            </div>
                        @endif
                        <form action="admin/thongtindiemdo/them" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div class="form-group">
                                <label>Vị Trí</label>
                                <select class="form-control" name="tinhtrang">
                                    @foreach($tinhtrang as $tt)
                                    <option value="{{$tt->id}}">{{$tt->id}}</option>
                                   @endforeach
                                </select>
                            </div>
                            
                            
                            <div class="form-group">
                                <label>Tên Điểm Đo</label>
                                <input class="form-control" name="tendiemdo" placeholder="Nhập Tên Điểm Đo" />
                            </div>
                            <div class="form-group">
                                <label>Mô Tả</label>
                                <input class="form-control" name="mota" placeholder="Nhập Mô Tả" />
                            </div>
                            
                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm Mới</button>
                    
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection