<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TinTuc extends Model
{
    //
    protected $table ="tintuc";
    public function loaisanpham()
    {
    	return $this-> belongsTo('App\LoaiSanPham','idloaisanpham','id');
    }
    public function comment()
    {
    	return $this-> hasMany('App\Comment','idtintuc','id');
    }
}
