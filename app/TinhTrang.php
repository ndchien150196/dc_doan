<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TinhTrang extends Model
{
     protected $table ="tinhtrang";
    public function thongtin()
    {
    	return $this-> hasMany('App\ThongTinDiemDo','idtinhtrang','id');
    }
    public function ketqua()
    {
    	return $this-> hasManyThrough('App\KetQuaDo','App\ThongTinDiemDo','idtinhtrang','idthongtin','id');
    }
}
