<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SanPham extends Model
{
    //
    protected $table ="sanpham";
    public function loaisanpham()
    {
    	return $this-> hasMany('App\LoaiSanPham','idsanpham','id');
    }
    public function tintuc()
    {
    	return $this-> hasManyThrough('App\TinTuc','App\LoaiSanPham','idsanpham','idloaisanpham','id');
    }
}
