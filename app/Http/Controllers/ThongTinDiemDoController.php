<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\TinhTrang;
use App\ThongTinDiemDo;

class ThongTinDiemDoController extends Controller
{
    //
    public function getDanhSach()
    {
    	$thongtindiemdo= thongtindiemdo::all();
    	return view('admin.thongtindiemdo.danhsach',['thongtindiemdo'=>$thongtindiemdo]);
    }

    public function getThem()
    {
        $tinhtrang= tinhtrang::all();
        return view('admin.thongtindiemdo.them',['tinhtrang'=>$tinhtrang]);

    }
    public function postThem(Request $request)
    {
       $this->validate($request,[

                'mota'=> 'required',
                'tendiemdo'=> 'required'
            ],[
                'tendiemdo.required'=>'Bạn chưa nhập tên điểm đo',
                'mota.required'=>'Bạn chưa nhập mô tả'
            ]);
        $thongtindiemdo= new thongtindiemdo;
        $thongtindiemdo->tendiemdo=$request->tendiemdo;
        $thongtindiemdo->mota= $request->mota;
        $thongtindiemdo->idtinhtrang = $request->tinhtrang;
        $thongtindiemdo->save();
        return redirect('admin/thongtindiemdo/them')->with('thongbao','Bạn đã thêm thành công');

    }
    public function getSua($id)
    {
    	$tinhtrang=tinhtrang::all();
        $thongtindiemdo=thongtindiemdo::find($id);
        return view('admin.thongtindiemdo.sua',['thongtindiemdo'=>$thongtindiemdo,'tinhtrang'=>$tinhtrang]);
    }
    public function postSua(Request $request,$id)
    {
         
    	 $this->validate($request,[

                'mota'=> 'required',
                'tendiemdo'=> 'required'
            ],[
                'tendiemdo.required'=>'Bạn chưa nhập tên điểm đo',
                'mota.required'=>'Bạn chưa nhập mô tả'
            ]);
        $thongtindiemdo =  thongtindiemdo::find($id);
        $thongtindiemdo->tendiemdo=$request->tendiemdo;
        $thongtindiemdo->mota= $request->mota;
        $thongtindiemdo->idtinhtrang = $request->tinhtrang;
        $thongtindiemdo->save();
        return redirect('admin/thongtindiemdo/sua/'.$id)->with('thongbao','Bạn đã sửa thành công');
    
    }
    public function getXoa($id)
    {
        $thongtindiemdo= thongtindiemdo::find($id);
        $thongtindiemdo->delete();
        return redirect('admin/thongtindiemdo/danhsach')->with('thongbao','Xóa Thành Công ');
    }
}