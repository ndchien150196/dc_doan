<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Comment;
use App\TinTuc;
use Illuminate\Support\Facades\Auth;
class CommentController extends Controller
{
    public function  getXoa($id,$idtintuc)
    {
        $comment =  Comment::find($id);
    	$comment -> delete();
    	return redirect('admin/tintuc/sua/'.$idtintuc)->with('thongbao','Xóa bình luận thành Công');
    }
    public function postcomment($id,Request $request)
    {
    	$idtintuc = $id;
    	$tintuc = tintuc::find($id);
    	$comment = new Comment;
    	$comment->idtintuc = $idtintuc;
    	$comment->iduser = Auth::user()->id;
    	$comment->NoiDung = $request->NoiDung;
    	$comment->save();

    	return redirect("tintuc/$id/".$tintuc->TieuDeKhongDau.".html")->with('thongbao','Viết bình luận thành công');
    }
}
