<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SanPham;
use App\LoaiSanPham;
class AjaxController extends Controller
{
    //
     public function getLoaiSanPham($idsanpham)
    {
    	$loaisanpham = LoaiSanPham::where('idsanpham',$idsanpham)->get();
    	foreach ($loaisanpham as $lsp)
    	{
    		echo "<option value='".$lsp->id."'>".$lsp->Ten."</option>";
    	}
    }
}
?>