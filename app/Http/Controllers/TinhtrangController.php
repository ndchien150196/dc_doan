<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\TinhTrang;
class TinhTrangController extends Controller
{
    //
   public function getDanhSach()
    {
    	
    	$tinhtrang= tinhtrang::all();
    	return view('admin.tinhtrang.danhsach',['tinhtrang'=>$tinhtrang]);
    }

    public function getThem()
    {
    	return view('admin.tinhtrang.them');
    }
    public function postThem(Request $request)
    {
    	

           $this->validate($request,[
            'mota'=>'required',
        ],[
            'mota.required'=>'Bạn chưa nhập mô tả',
        ]);
        $tinhtrang  = new tinhtrang;
        $tinhtrang->dht11= $request->dht11;
        $tinhtrang ->spec= $request->spec;
        $tinhtrang->dust= $request->dust;      
        $tinhtrang->bh1750= $request->bh1750;
        $tinhtrang->mota= $request->mota;

        
        $tinhtrang->save();
        return redirect('admin/tinhtrang/them')->with('thongbao','Thêm Tình Trạng Thành Công');
    }

    public function getSua($id)
    {
    	$tinhtrang = tinhtrang::find($id);
        return view('admin.tinhtrang.sua',['tinhtrang'=>$tinhtrang]);
    }
    public function postSua(Request $request,$id)
    {   
        $tinhtrang = tinhtrang::find($id);
    	 $this->validate($request,[
            'mota'=>'required',
        ],[
            'mota.required'=>'Bạn chưa nhập mô tả',
        ]);
        $tinhtrang->dht11= $request->dht11;
        $tinhtrang ->spec= $request->spec;
        $tinhtrang->dust= $request->dust;      
        $tinhtrang->bh1750= $request->bh1750;
        $tinhtrang->mota= $request->mota;
        $tinhtrang->save();
        return redirect('admin/tinhtrang/sua/'.$id)->with('thongbao','Sửa Thành Công');
    }
    
    public function getXoa($id)
    {
    	$tinhtrang= tinhtrang::find($id);
    	$tinhtrang->delete();
    	return redirect('admin/tinhtrang/danhsach')->with('thongbao','Xóa Thành Công ');
    }
}