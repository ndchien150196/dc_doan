<?php

namespace App\Http\Controllers;use Illuminate\Http\Request;
use App\Http\Requests;
use App\SanPham;
use App\Slide;
use App\LoaiSanPham;
use App\TinTuc;
use App\User;
use App\TinhTrang;
use App\ThongTinDiemDo;
use App\KetQuaDo;
use Illuminate\Support\Facades\Auth;
class PageController extends Controller
{
	function __construct()
	{  
        $tinhtrang=tinhtrang::all();
		$sanpham = sanpham::all();
		$slide= slide::all();
		view()->share('sanpham',$sanpham);
		view()->share('slide',$slide);

        if(Auth::check())
            {
                view()->share('nguoidung',Auth::user());
            }   
	}
    function trangchu()
    {
    	//$sanpham = sanpham::all();
    	return view('pages.trangchu');//,['sanpham'=>$sanpham]
    }
    function lienhe()
    {
    	//$sanpham = sanpham::all();
    	return view('pages.lienhe');//,['sanpham'=>$sanpham]
    }
     function loaisanpham($id)
    {
    	$loaisanpham = loaisanpham::find($id);
    	$tintuc =tintuc::where('idloaisanpham',$id)->paginate(5);
    	return view('pages.loaisanpham',['loaisanpham'=>$loaisanpham,'tintuc'=>$tintuc]);
    }
    function tintuc($id)
    {
    	
    	$tintuc =tintuc::find($id);
    	$tinnoibat = tintuc::where('NoiBat',1)->take(4)->get();
    	$tinlienquan = tintuc::where('idloaisanpham',$tintuc->idloaisanpham)->take(4)->get();
    	return view('pages.tintuc',['tintuc'=>$tintuc,'tinnoibat'=>$tinnoibat,'tinlienquan'=>$tinlienquan]);
    }
    function getdangnhap()
    {
      
        return view('pages.dangnhap');
    }
    function postdangnhap(Request $request)
    {
        
        $this->validate($request,[
            
            'email'=>'required|email',
            'password'=>'required|min:6|max:32',
        ],[
            
            'email.required'=>'Bạn chưa nhập email', 
            'email.email'=>'Bạn chưa nhập đúng định dạng email',
            'password.required'=>'Bạn chưa nhập mật khẩu',
            'password.min'=>'Mật khẩu phải có ít nhất 6 ký tự',
            'password.max'=>'Mật khẩu phải có tối đa 32 ký tự'
            
        ]);
         if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
        {
        return redirect('trangchu');
        }
        else
        {
            return redirect('dangnhap')->with('thongbao','Đăng nhập không thành công');
        }
    }
    function getdangxuat()
    {
        Auth::logout();
        return redirect('trangchu');
    }
    function getnguoidung()
    {
        $user =Auth::user();
        return view('pages.nguoidung',['nguoidung'=>$user]);
    }
    function postnguoidung(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|min:6',
        ],[
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'name.min'=>'Tên quá ngắn,nó phải có ít nhất 6 ký tự',
            
        ]);
        $user  = Auth::user();
        $user->name= $request->name;
        if($request -> changePassword =="on")
        {
            $this->validate($request,[
            
            'password'=>'required|min:6|max:32',
            'passwordAgain'=>'required|same:password'

        ],[
            
            
            'password.required'=>'Bạn chưa nhập mật khẩu',
            'password.min'=>'Mật khẩu phải có ít nhất 6 ký tự',
            'password.max'=>'Mật khẩu phải có tối đa 32 ký tự',
            'passwordAgain.required'=>'Mật chưa nhập lại mật khẩu',
            'passwordAgain.same'=>'Mật khẩu chưa trùng khớp'
        ]);
        $user ->password= bcrypt($request->password);
        }
        $user->save();
        return redirect('nguoidung')->with('thongbao','Sửa User Thành Công');
    }
    function getdangky()
    {
        return view('pages.dangky');
    }
    function postdangky(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|min:6',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:6|max:32',
            'passwordAgain'=>'required|same:password'

        ],[
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'email.required'=>'Bạn chưa nhập email',
            'name.min'=>'Tên quá ngắn,nó phải có ít nhất 6 ký tự',
            'email.email'=>'Bạn chưa nhập đúng định dạng email',
            'email.uniqid()'=>'Email đã tông tại',
            'password.required'=>'Bạn chưa nhập mật khẩu',
            'password.min'=>'Mật khẩu phải có ít nhất 6 ký tự',
            'password.max'=>'Mật khẩu phải có tối đa 32 ký tự',
            'passwordAgain.required'=>'Mật chưa nhập lại mật khẩu',
            'passwordAgain.same'=>'Mật khẩu chưa trùng khớp'
        ]);
        $user  = new User;
        $user->name= $request->name;
    
        $user ->email= $request->email;
        $user ->password= bcrypt($request->password);
        $user->quyen= 0;

        
        $user->save();
        return redirect('dangnhap')->with('thongbao','Chúc Mừng Bạn Đăng Ký Thành Công');
    }
    function timkiem(Request $request)
    {   
        $tukhoa = $request->tukhoa;
        $tintuc= tintuc::where('TieuDe','like',"%$tukhoa%")->orWhere('TomTat','like',"%$tukhoa%")->orWhere('NoiDung','like',"%$tukhoa%")->take(30)->paginate(5);
        return view('pages.timkiem',['tintuc'=>$tintuc,'tukhoa'=>$tukhoa]);
    }
    function thongso(Request $request)
    {
             $ketquado = ketquado::all() ;
            return view('pages.thongso',['ketquado'=>$ketquado]);

    }
    function teeee(Request $request)
    {
    $users=$request->id;
    DB::table('users')->where('id', '=', id)->get();
    echo "id==2";   
    }
    function gioithieu()
    {
    return view('pages.gioithieu');
    }
}
