<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\SanPham;
class SanPhamController extends Controller
{
    //
   public function getDanhSach()
    {
    	$sanpham= sanpham::all();
    	return view('admin.sanpham.danhsach',['sanpham'=>$sanpham]);
    }

    public function getThem()
    {
    	return view('admin.sanpham.them');
    }
    public function postThem(Request $request)
    {
    	$this->validate($request,
    		[
    			'Ten'=> 'required|min:3|max:100|unique:SanPham,Ten'
    		],
    		[
    			'Ten.required'=>'Bạn chưa nhập tên sản phẩm',
    			'Ten.unique'=>'Tên sản phẩm đã tồn tại',
    			'Ten.min'=>'Tên sản phẩm quá ngắn,nó phải có độ dài từ 3 đến 100 ký tự',
    			'Ten.max'=>'Tên sản phẩm quá dài,nó phải có độ dài từ 3 đến 100 ký tự',
    		]);
    	$sanpham = new SanPham;
    	$sanpham ->Ten= $request->Ten;
    	$sanpham ->TenKhongDau=changeTitle($request->Ten);
    	$sanpham->save();
    	return redirect('admin/sanpham/them')->with('thongbao','Thêm Thành Công');

    }

    public function getSua($id)
    {
    	$sanpham = sanpham::find($id);
    	return view('admin.sanpham.sua',['sanpham'=>$sanpham]);
    }
    public function postSua(Request $request,$id)
    {
    	$sanpham = sanpham::find($id);
    	$this->validate($request,
    		[
    			'Ten'=> 'required|unique:SanPham,Ten|min:3|max:100'
    		],
    		[
    			'Ten.required'=>'Bạn chưa nhập tên sản phẩm',
    			'Ten.unique'=>'Tên sản phẩm đã tồn tại',
    			'Ten.min'=>'Tên sản phẩm quá ngắn,nó phải có độ dài từ 3 đến 100 ký tự',
    			'Ten.max'=>'Tên sản phẩm quá dài,nó phải có độ dài từ 3 đến 100 ký tự',
    		]
    	);
    	$sanpham->Ten=$request->Ten;
    	$sanpham->TenKhongDau= changeTitle($request->Ten);
    	$sanpham->save();
    	return redirect('admin/sanpham/sua/'.$id)->with('thongbao','Sửa Thành Công');
    }
    public function getXoa($id)
    {
    	$sanpham= sanpham::find($id);
    	$sanpham->delete();
    	return redirect('admin/sanpham/danhsach')->with('thongbao','Xóa Thành Công ');
    }
}