<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\SanPham;
use App\LoaiSanPham;

class LoaiSanPhamController extends Controller
{
    //
    public function getDanhSach()
    {
    	$loaisanpham= loaisanpham::all();
    	return view('admin.loaisanpham.danhsach',['loaisanpham'=>$loaisanpham]);
    }

    public function getThem()
    {
        $sanpham= sanpham::all();
        return view('admin.loaisanpham.them',['sanpham'=>$sanpham]);
    }
    public function postThem(Request $request)
    {
        $this->validate($request,[

                'Ten'=> 'required|min:1|max:100|unique:LoaiSanPham,Ten',
                'sanpham'=> 'required'
            ],[
                'Ten.required'=>'Bạn chưa nhập tên loại sản phẩm',
                'Ten.uniqure'=>'Tên loại sản phẩm đã tồn tại',
                'Ten.min'=>'Tên sản phẩm quá ngắn,nó phải có độ dài từ 1 đến 100 ký tự',
                'Ten.max'=>'Tên sản phẩm quá dài,nó phải có độ dài từ 1 đến 100 ký tự',
                'SanPham.required'=>'Bạn chưa chọn sản phẩm'
            ]);
        $loaisanpham= new loaisanpham;
        $loaisanpham->Ten=$request->Ten;
        $loaisanpham->TenKhongDau= changeTitle($request->Ten);
        $loaisanpham->idsanpham = $request->sanpham;
        $loaisanpham->save();
        return redirect('admin/loaisanpham/them')->with('thongbao','Bạn đã thêm thành công');

    }

    public function getSua($id)
    {
        $sanpham=sanpham::all();
        $loaisanpham=LoaiSanPham::find($id);
        return view('admin.loaisanpham.sua',['loaisanpham'=>$loaisanpham,'sanpham'=>$sanpham]);
    }
    public function postSua(Request $request,$id)
    {
         $this->validate($request,[

                'Ten'=> 'required|unique:LoaiSanPham,Ten|min:1|max:100',
                'sanpham'=> 'required'
            ],[
                'Ten.required'=>'Bạn chưa nhập tên loại sản phẩm',
                'Ten.uniqure'=>'Tên loại sản phẩm đã tồn tại',
                'Ten.min'=>'Tên sản phẩm quá ngắn,nó phải có độ dài từ 1 đến 100 ký tự',
                'Ten.max'=>'Tên sản phẩm quá dài,nó phải có độ dài từ 1 đến 100 ký tự',
                'SanPham.required'=>'Bạn chưa chọn sản phẩm'
            ]);
        $loaisanpham =  loaisanpham::find($id);
        $loaisanpham->Ten = $request->Ten;
        $loaisanpham->TenKhongDau = changeTitle($request->Ten);
        $loaisanpham->idsanpham = $request->sanpham;
        $loaisanpham->save();
        return redirect('admin/loaisanpham/sua/'.$id)->with('thongbao','Bạn đã sửa thành công');

    
    }
    public function getXoa($id)
    {
        $loaisanpham= loaisanpham::find($id);
        $loaisanpham->delete();
        return redirect('admin/loaisanpham/danhsach')->with('thongbao','Xóa Thành Công ');
    }
}