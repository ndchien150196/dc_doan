<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\TinhTrang;
use App\ThongTinDiemDo;
use App\KetQuaDo;

class KetQuaDoController extends Controller
{
     public function getDanhSach()
    {
    	$ketquado= ketquado::orderBy('id','DESC')->get();
    	return view('admin.ketquado.danhsach',['ketquado'=>$ketquado]);
    }

    public function getThem()
    {
    	$tinhtrang = tinhtrang::all();
    	$thongtindiemdo = thongtindiemdo::all();
    	return view('admin.ketquado.them',['tinhtrang'=>$tinhtrang,'thongtindiemdo'=>$thongtindiemdo]);
    }
    public function postThem(Request $request)
    {
    	$this->validate($request,[
    		'nhietdo'=>'required',
    		'doam'=>'required',
    		'co'=>'required',
    		'buipm1'=>'required',
    		'buipm25'=>'required',
    		'pin'=>'required',
    		'mota'=>'required',
    		'anhsang'=>'required'

    	],[
    		'nhietdo.required'=>'Bạn chưa nhập nhiệt độ',
    		'doam.required'=>'Bạn chưa nhập độ ẩm',
    		'co.required'=>'Bạn chưa nhập co ',
    		'buipm1.required'=>'Bạn chưa nhập bụi pm1 ',
    		'buipm25.required'=>'Bạn chưa nhập bụi pm2.5',
    		'pin.required'=>'Bạn chưa nhập pin',
    		'mota.required'=>'Bạn chưa nhập mô tả',
    		'anhsang.required'=>'Bạn chưa nhập ánh sáng'
    	]);
    	$ketquado = new ketquado;
    	$ketquado ->idthongtindiemdo= $request->thongtindiemdo;
    	$ketquado ->nhietdo= $request->nhietdo;	
    	$ketquado ->doam= $request->doam;
    	$ketquado ->anhsang= $request->anhsang;
    	$ketquado ->co= $request->co;
        $ketquado->buipm1= $request->buipm1;
        $ketquado->buipm25= $request->buipm25;
        $ketquado->pin= $request->pin;
        $ketquado->mota= $request->mota;
    	$ketquado->save();
    	return redirect('admin/ketquado/them')->with('thongbao','Thêm Tin Thành Công');


    }
    public function getSua($id)
    {
    	$tinhtrang=tinhtrang::all();
    	$thongtindiemdo=thongtindiemdo::all();
    	$ketquado= ketquado::find($id);
    	return view('admin.ketquado.sua',['ketquado'=>$ketquado,'tinhtrang'=>$tinhtrang,'thongtindiemdo'=>$thongtindiemdo]);
    }
    public function postSua(Request $request,$id)
    {	
    	$ketquado= ketquado::find($id);
    	$this->validate($request,[
    		'nhietdo'=>'required',
    		'doam'=>'required',
    		'co'=>'required',
    		'buipm1'=>'required',
    		'buipm25'=>'required',
    		'pin'=>'required',
    		'mota'=>'required',
    		'anhsang'=>'required'

    	],[
    		'nhietdo.required'=>'Bạn chưa nhập nhiệt độ',
    		'doam.required'=>'Bạn chưa nhập độ ẩm',
    		'co.required'=>'Bạn chưa nhập co ',
    		'buipm1.required'=>'Bạn chưa nhập bụi pm1 ',
    		'buipm25.required'=>'Bạn chưa nhập bụi pm2.5',
    		'pin.required'=>'Bạn chưa nhập pin',
    		'mota.required'=>'Bạn chưa nhập mô tả',
    		'anhsang.required'=>'Bạn chưa nhập ánh sáng'
    	]);
    	$ketquado ->idthongtindiemdo= $request->thongtindiemdo;
    	$ketquado ->nhietdo= $request->nhietdo;	
    	$ketquado ->doam= $request->doam;
    	$ketquado ->anhsang= $request->anhsang;
    	$ketquado ->co= $request->co;
        $ketquado->buipm1= $request->buipm1;
        $ketquado->buipm25= $request->buipm25;
        $ketquado->pin= $request->pin;
        $ketquado->mota= $request->mota;
    	$ketquado->save();
    	return redirect('admin/ketquado/sua/'.$id)->with('thongbao','Sửa Thành Công');
    }
    public function  getXoa($id)
    {
    	$ketquado =  ketquado::find($id);
    	$ketquado -> delete();
    	return redirect('admin/ketquado/danhsach')->with('thongbao','Xóa thành Công');
    }
}
