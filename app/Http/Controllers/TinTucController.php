<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SanPham;
use App\TinTuc;
use App\LoaiSanPham;
use App\Comment;
class TinTucController extends Controller
{
    //
   public function getDanhSach()
    {
    	$tintuc= tintuc::orderBy('id','DESC')->get();
    	return view('admin.tintuc.danhsach',['tintuc'=>$tintuc]);
    }

    public function getThem()
    {
    	$sanpham =  sanpham::all();
    	$loaisanpham = loaisanpham::all();
    	return view('admin.tintuc.them',['sanpham'=>$sanpham,'loaisanpham'=>$loaisanpham]);
    }
    public function postThem(Request $request)
    {
    	$this->validate($request,[
    		'loaisanpham'=>'required',
    		'TieuDe'=>'required|min:3|unique:tintuc,TieuDe',
    		'TomTat'=>'required',
    		'NoiDung'=>'required'

    	],[
    		'loaisanpham.required'=>'Bạn chưa chọn loại sản phẩm',
    		'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
    		'TieuDe.min'=>'Tên sản phẩm quá ngắn,nó phải có ít nhất 3 ký tự',
    		'TieuDe.unique'=>'Tiêu Đề đã tồn tại',
    		'TomTat.required'=>'Bạn chưa nhập tóm tắt',
    		'NoiDung.required'=>'Bạn chưa nhập nội dung'
    	]);
    	$tintuc = new TinTuc;
    	$tintuc ->TieuDe= $request->TieuDe;
    	$tintuc ->TieuDeKhongDau=changeTitle($request->TieuDe);
    	$tintuc ->idloaisanpham= $request->loaisanpham;
    	$tintuc ->TomTat= $request->TomTat;
    	$tintuc ->NoiDung= $request->NoiDung;
        $tintuc->NoiBat= $request->NoiBat;
    	$tintuc->SoLuotXem =0;
    	if($request->hasFile('Hinh'))
    	{
    		$file=$request->file('Hinh');
    		$duoi =$file->getClientOriginalExtension();
    		if($duoi !='jpg' && $duoi !='png' && $duoi !='jpeg')
    		{
    			return redirect('admin/tintuc/them')->with('loi','Bạn chỉ được chọn file jpg,png,jpeg');
    		}
    		$name=$file->getClientOriginalName();
    		$Hinh=str_random(4)."_".$name;
    		while (file_exists("upload/tintuc/".$Hinh)) {
    			$Hinh=str_random(4)."_".$name;
    		}
    		$file->move("upload/tintuc",$Hinh);
    		$tintuc->Hinh=$Hinh;
    	}
    	else
    	{
    		$tintuc->Hinh="";
    	}
    	$tintuc->save();
    	return redirect('admin/tintuc/them')->with('thongbao','Thêm Tin Thành Công');

    }
    public function getSua($id)
    {
    	$sanpham=sanpham::all();
    	$loaisanpham=loaisanpham::all();
    	$tintuc= tintuc::find($id);
    	return view('admin.tintuc.sua',['tintuc'=>$tintuc,'sanpham'=>$sanpham,'loaisanpham'=>$loaisanpham]);
    }
    public function postSua(Request $request,$id)
    {	
    	$tintuc= tintuc::find($id);
    	$this->validate($request,[
    		'loaisanpham'=>'required',
    		'TieuDe'=>'required|min:3|unique:tintuc,TieuDe',
    		'TomTat'=>'required',
    		'NoiDung'=>'required'

    	],[
    		'loaisanpham.required'=>'Bạn chưa chọn loại sản phẩm',
    		'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
    		'TieuDe.min'=>'Tên sản phẩm quá ngắn,nó phải có ít nhất 3 ký tự',
    		'TieuDe.unique'=>'Tiêu Đề đã tồn tại',
    		'TomTat.required'=>'Bạn chưa nhập tóm tắt',
    		'NoiDung.required'=>'Bạn chưa nhập nội dung'
    	]);
    	$tintuc ->TieuDe= $request->TieuDe;
    	$tintuc ->TieuDeKhongDau=changeTitle($request->TieuDe);
    	$tintuc ->idloaisanpham= $request->loaisanpham;
    	$tintuc ->TomTat= $request->TomTat;
    	$tintuc ->NoiDung= $request->NoiDung;
        $tintuc->NoiBat= $request->NoiBat;
    
    	if($request->hasFile('Hinh'))
    	{
    		$file=$request->file('Hinh');
    		$duoi =$file->getClientOriginalExtension();
    		if($duoi !='jpg' && $duoi !='png' && $duoi !='jpeg')
    		{
    			return redirect('admin/tintuc/them')->with('loi','Bạn chỉ được chọn file jpg,png,jpeg');
    		}
    		$name=$file->getClientOriginalName();
    		$Hinh=str_random(4)."_".$name;
    		while (file_exists("upload/tintuc/".$Hinh)) {
    			$Hinh=str_random(4)."_".$name;
    		}

    		$file->move("upload/tintuc",$Hinh);
    		//unlink("upload/tintuc/".$tintuc->Hinh);
    		$tintuc->Hinh=$Hinh;
    	}
    	
    	$tintuc->save();
    	return redirect('admin/tintuc/sua/'.$id)->with('thongbao','Sửa thành công');
    }
    public function  getXoa($id)
    {
    	$tintuc =  tintuc::find($id);
    	$tintuc -> delete();
    	return redirect('admin/tintuc/danhsach')->with('thongbao','Xóa thành Công');
    }
}