<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiSanPham extends Model
{
    //
    protected $table ="loaisanpham";
    public function sanpham()
    {
    	return $this-> belongsTo('App\SanPham','idsanpham','id');
    }
    public function tintuc()
    {
    	return $this-> hasMany('App\TinTuc','idloaisanpham','id');
    }
}
