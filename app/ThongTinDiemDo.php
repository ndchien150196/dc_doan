<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThongTinDiemDo extends Model
{
     protected $table ="thongtindiemdo";
    public function tinhtrang()
    {
    	return $this-> belongsTo('App\TinhTrang','idtinhtrang','id');
    }
    public function ketquado()
    {
    	return $this-> hasMany('App\KetQuaDo','idthongtindiemdo','id');
    }
}
